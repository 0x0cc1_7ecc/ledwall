rootPath="/home/pi/ledWall/video"

workDir=`pwd`
rootPath="${workDir}/video"

imagePath="${workDir}/image"
videosPath="${rootPath}/toConvert"
oldVideosPath="${rootPath}/converted"
videoPath="${rootPath}/"
destPath="${rootPath}/new"



if  test "$1" = ""
then
	echo " LedWall video Convert script, uses ffmpeg"
	echo " usage: ./convert video <path> <'crop' to crop video >"
	echo " usage: ./convert video-dl <url> "
	echo " usage: ./convert image <path> "
	echo " usage: ./convert image-dl <path> "
	echo " usage: ./convert multi"
else


if test "$1" = "multi"
	then
		

		for entry in "${videosPath}"/*
		do
			echo " started video conversion $entry"
			eext=$(basename "$entry")
			ext="${eext##*.}"
			#echo "${rootPath}/tmp.${ext}"
			if test "$3" = "crop"
			then
				# 850x480 => 300:80:100:300 sur 60*5 leds
				ffmpeg  -i "$entry" -ss 00:00:20 -t 00:01:20 -an  -f  crop=300:80:100:300 "${videosPath}/tmp.${ext}" -y
			else
				cp "$entry" "${videosPath}/tmp.${ext}"
			fi

			declare -i vidNumb=1
			vidN=""
			printf -v vidN "%02d" $vidNumb
			while [ -d "${destPath}/vid${vidN}" ]
				do	
					#echo "vid${vidN} existe"	
					((vidNumb++))
					printf -v vidN "%02d" $vidNumb
						
				done
			mkdir "${destPath}/vid${vidN}"
			echo "dir ${destPath}/vid${vidN} was created"
			
			ffmpeg -i "${videosPath}/tmp.${ext}" -ss 00:00:00 -t 00:02:00 -an -r 12 -vf scale=60:40 "${destPath}/vid${vidN}/img-%05d.png"
			rm "${videosPath}/tmp.${ext}"
			mv ${entry} ${oldVideosPath}
		done

		
	fi

if  test "$2" = ""
	then
		echo " please provide a path as argument 2"
	else  
		if test "$1" = "video-dl"
		then
			echo " started video download @ $2"
			cd ${rootPath}
			wget $2

			$1="video"
			cd ${workDir}
		fi

		if test "$1" = "video"
		then
			

			echo " started video conversion $2"
			eext=$(basename "$2")
			ext="${eext##*.}"
			#echo "${rootPath}/tmp.${ext}"
			if test "$3" = "crop"
			then
				# 850x480 => 300:80:100:300 sur 60*5 leds
				ffmpeg  -i "$2" -ss 00:00:20 -t 00:01:20 -an  -f  crop=300:80:100:300 "${videoPath}/tmp.${ext}" -y
			else
				cp "$2" "${videoPath}/tmp.${ext}"
			fi

			declare -i vidNumb=1
			vidN=""
			printf -v vidN "%02d" $vidNumb
			while [ -d "${destPath}/vid${vidN}" ]
				do	
					#echo "vid${vidN} existe"	
					((vidNumb++))
					printf -v vidN "%02d" $vidNumb
						
				done
			mkdir "${destPath}/vid${vidN}"
			echo "dir ${destPath}/vid${vidN} was created"
			
			ffmpeg -i "${videoPath}/tmp.${ext}" -ss 00:00:00 -t 00:02:00 -an -r 24 -vf scale=60:29 "${destPath}/vid${vidN}/img-%05d.png"
			#rm ${rootPath}/tmp.${ext}

		fi

		

		if test "$1" = "image-dl"
		then

			echo " started image download @ $2"
			cd ${rootPath}
			wget $2

			$1 = "image"
			cd ${workDir}
		fi

		if test "$1" = "image"
		then
			echo " started image conversion $2"
			
			echo "${2%.*}"
			convert "$2" -resize 60x3! ${imagePath}/imgtemp.png
			vidNumb=001
			while [ -f "${imagePath}/img${vidNumb}.png" ]
				do	
					#echo "vid${vidNumb} existe"	
					((vidNumb++))
					printf -v vidNumb "%03d" $vidNumb
						
				done
			echo "${imagePath}/img${vidNumb}.png"
			mv ${imagePath}/imgtemp.png ${imagePath}/img${vidNumb}.png
		fi

		

	fi

fi

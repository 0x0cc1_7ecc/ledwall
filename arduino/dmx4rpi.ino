#include <DMXSerial.h>
#include <SoftwareSerial.h>

SoftwareSerial softSerial(10, 11); // RX, TX
int DMXaddress=1;
int DMXaddressCg=16;
const int DMXrange=17;
byte DMXbuffer[DMXrange];
String DMXstring="";


static int p1=5;
int t=0;


void read_dmx2() {
 byte DMXrxMsg[DMXrange];
  
  for(int i = 0; i < DMXrange; i++) {
    
    DMXrxMsg[i]=DMXSerial.read(DMXaddress+i);
    
      if (DMXrxMsg[i] != DMXbuffer[i]) {
        
        DMXbuffer[i]=DMXrxMsg[i];
        
        softSerial.println("DMX["+String(DMXaddress+i)+"] = " + String(DMXrxMsg[i]));
        delay(500);
      }
  }
}

void init_dmx() {
  for(int i = 0; i < DMXrange; i++) {
    
    DMXbuffer[i]=DMXSerial.read(DMXaddress+i);
  }
}

void read_dmx() {
  // DMX implementation:
  // channel 1 : brightness     0 - 255
  // channel 2 : program        0 - 239 : videos, 240: VUmeter 241: SPAn 242: Osc, 250:circles 251:sine
  // channel 3 : Red 1          0 - 255
  // channel 4 : Green 1        0 - 255
  // channel 5 : Blue 1         0 - 255
  // channel 6 : Red 2          0 - 255
  // channel 7 : Green 2        0 - 255
  // channel 8 : Blue 2         0 - 255
  // channel 9 : Message On/off 0 - 255
  // channel10 : Message string 0 - 255 characters ASCII number, start with 1, ends with 0
  // channel11 : Parameter 1    0 - 255  : usually time
  // channel12 : Parameter 2    0 - 255  
  // channel13 : Parameter 3    0 - 255  
  // channel14 : Parameter 4    0 - 255  
  // channel15 : Parameter 5    0 - 255  
  // channel16 : Enable DMX adress change 0-255  
  // channel17 : DMX Adress 0 -255  
  

  byte DMXrxMsg[DMXrange];
  // Read all DMX channels
  for(int i = 0; i < DMXrange; i++) {
    
    DMXrxMsg[i]=DMXSerial.read(DMXaddress+i);
    
      
    if (DMXrxMsg[i] != DMXbuffer[i]) {
      //softSerial.println("DMX["+String(DMXaddress+i)+"] = " + String(DMXrxMsg[i])); 
      digitalWrite(13,HIGH);
      DMXbuffer[i]=DMXrxMsg[i];
       switch (i) {
        case 0: // brightness
          softSerial.println("BR"+String(DMXrxMsg[i]));
        break;
        case 1: // program
          if  (DMXrxMsg[i] == 0) {
             softSerial.println("DEMO");
          }
          if ( (DMXrxMsg[i] > 0) && ( DMXrxMsg[i] < 250) ) {
            softSerial.println("VL"+String(DMXrxMsg[i]));
          }
          if  (DMXrxMsg[i] == 250 ) {
             softSerial.println("DRAW1");
          }
          if  (DMXrxMsg[i] == 251 ) {
             softSerial.println("DRAW2");
          }
           if  (DMXrxMsg[i] == 252 ) {
             softSerial.println("DRAW3");
          }
          if  (DMXrxMsg[i] == 253 ) {
             softSerial.println("DRAW4");
          }
        break;
        case 2: // Red1
          softSerial.println("TC"+String(DMXbuffer[i])+"," + String(DMXbuffer[i+1])+ "," + String(DMXbuffer[i+2]) );
        break;
        case 3: // Green1
          softSerial.println("TC"+String(DMXbuffer[i-1])+"," + String(DMXbuffer[i])+ "," + String(DMXbuffer[i+1]) );
        break;
        case 4: // Blue1
           softSerial.println("TC"+String(DMXbuffer[i-2])+"," + String(DMXbuffer[i-1])+ "," + String(DMXbuffer[i]) );
        break;
        case 5: // Red2
           softSerial.println("CG"+String(DMXbuffer[i])+"," + String(DMXbuffer[i+1])+ "," + String(DMXbuffer[i+2]) );
        break;
        case 6: // Green2
           softSerial.println("CG"+String(DMXbuffer[i-1])+"," + String(DMXbuffer[i])+ "," + String(DMXbuffer[i+1]) );
        break;
        case 7: // Blue2
            softSerial.println("CG"+String(DMXbuffer[i-2])+"," + String(DMXbuffer[i-1])+ "," + String(DMXbuffer[i]) );
        break;
        case 8: // Message On/off
            if (DMXrxMsg[i] == 255) {
              //if (DMXstring !="") {
                softSerial.println("TD"+DMXstring);
            //  }
            }
        break;
        case 9: // Message
          //  Message string 0 - 255 characters ASCII number, start with 1, ends with 0
          String DMXstr="";
          int d=350;
          int c;
          if (DMXrxMsg[i] == 1) {
           while (DMXSerial.read(DMXaddress+i) == 1) {           
            delay(d);
           }
            while  (DMXSerial.read(DMXaddress+i) != 0) {
              c=DMXSerial.read(DMXaddress+i);
              DMXstr=DMXstr + String(char(c));
              while (DMXSerial.read(DMXaddress+i) == c) {
                delay(d);
                }
              //delay(d*3); 
            }
             //softSerial.println("TD = "+DMXstr);
             DMXstring=DMXstr;            
          }

        break;
        case 10: // parameter 1 change
          softSerial.println("PAR1");
          softSerial.println("PC"+String(i-9)+","+String(DMXrxMsg[i]));
        break;
        case 11: // parameter 2 change
          softSerial.println("PC"+String(i-9)+","+String(DMXrxMsg[i]));
        break;
        case 12: // parameter 3 change
          softSerial.println("PC"+String(i-9)+","+String(DMXrxMsg[i]));
        break;
        case 13: // parameter 4 change
          softSerial.println("PC"+String(i-9)+","+String(DMXrxMsg[i]));
        break;
        case 14: // parameter 5 change
          softSerial.println("PC"+String(i-9)+","+String(DMXrxMsg[i]));
        break;
       }
       if (i>=10 && i<=14) { // parameter 1-5 change
          softSerial.println("PC"+String(i-9)+","+String(DMXrxMsg[i]));
        }
       digitalWrite(13,LOW);
       delay(400);
    }

    
  }
  
/* for(int i = 0; i < DMXrange; i++) {
      softSerial.println("DMX "+String(DMXaddress+i)+" = " + String(DMXbuffer[i]));
    }*/
 


  
}

void setup() {
  //Serial.begin(9600);
  softSerial.begin(9600);
  DMXSerial.init(DMXReceiver);
  int t=100;
  DMXstring = "";
  pinMode(p1,OUTPUT);digitalWrite(p1,HIGH);delay(t);digitalWrite(p1,LOW);delay(t);digitalWrite(p1,HIGH);delay(t);digitalWrite(p1,LOW);delay(t);
}


void loop() {

  if (DMXSerial.read(DMXaddress+DMXaddressCg-1) == 255 ) {
    DMXaddress=DMXSerial.read(DMXaddress+DMXaddressCg);
    delay(2000);
  }
  
  read_dmx();
  
}

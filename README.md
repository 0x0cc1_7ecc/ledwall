.# LedWall

A ws2812b LEDs based display, with DMX support, controlled by a rasberry pi 3B+.

We wanted first to build the kind of ws2812 large displays as described in this project:
https://github.com/SolidStateGroup/rio
but after taking lot of time for a partial working result, we decided to do all the code from scratch.

Its basically a python script, using neopixel, numpy and  wave librairies.
The way videos are displayed is really basic: every video has been previously converted into 60*40 images (using the provided shell script), and the program loads picture one then two, etc,...
To be able to switch videos quickly, all videos have to be loaded by the program before playback.

The intensity of the leds can be changed in real time ,as well as the video played.
You can also display a scroling text while videos are playing, the message and its activation can be changed also in real time.

<img src="Build pictures/ledWall_build1.png ">

All these features are controllable via DMX, including color of the scrolling text and a few more parameters.
A QLC+ *.qxf fixture file and an example *.qxw workspace are provided.
A python script is also provided to convert scrolling text to a QLC+ script to turn them into DMX messages.

## Features:

- up to 2400 LEDs control (using 1 GPIO pin )
- Image display
- Video playback (requires conversion)
- Scrolling text
- Graphics generation (primitive)
- Remote control via DMX

## Hardware :

### electronics

- WS2812B led strip
- Raspberry pi 3 B+ at least, tested with a  rpi 2b+ that seemed too slow
- Arduino nano ATmega328p, but any arduino may be used as few libs and I/O pins are used
- MAX485 CPA1+ chip for DMX (RS485) bus 
- USB to UART module (maybe the pi UART may be used but needs level shifting to work w/ the arduino)
- Few Resistors
- header cables & 3 wire classic electric cable (diam TBC)
- USB mini & micro cables
- 3 * XLR 3 point connectors, 2 Female, 1 Male
- 5VDC power supplies, you can use desktop PC/MAC power supplies (1m /60 leds in our case= 3.6 Amps at full brightness)

### mechanical

- transparent polycarbonate panel to stick the led strips on
- transparent plexiglass panel to be placed in front of LEDs
- wood for front/rear frames
- wood for rear panel
- 3D printed box for RPI+arduino+MAX485
- IKEA klamtare 45x27x15 cm box for the power supplies
- metal bolts and fasteners

## Our prototype:

https://www.youtube.com/watch?v=1v6-Y5n7oTg

https://www.youtube.com/watch?v=lurk5F3etYk

## Installing/Running the ledwall python script:

Just copy the files into a folder, keeping the directories .

Make sure the following python librairies are installed:imageio, math, numpy, wave, serial

You need to fill your _videoPath_ with already converted videos, for this use the _convert.sh_ script.
The script is supposed to be run at the root of the ledwall folder, it will automatically read/put files in _video_ subfolder .
Usage of the script is detailed at the beginning of the script.
You can convert multiple video files using _./convert.sh multi_

Edit the _ledWall.py_ file if you need to modify:
 - LED_COUNT the more led you'll add, the slower the display will be. 2400 led was acceptable
 - LED_WIDTH 
 - LED_HEIGHT
 - LED_PIN
 - videoPath  : the absolute path to a folder with converted to img videos folder's inside
 - ser        : change to the /dev/xx of your USB/Uart adapter

You'll also have to load the _dmx4rpi_ firmware into your arduino.

## Build Instructions

### Rpi, Arduino & MAX485 Wiring

Rpi and arduino are conected via a USB/serial adapter on the pi to the software serial pins of the arduino.
MAX485 is connected to the arduino as the rpi cant handle the timings correctly.

<img src="Build pictures/general_wiring.png " width="400" height="600">

### Led / Power supplies Wiring

Wire the data output pin of one strip to the data input pin of the below strip.
To reduce the wire length you have to put the data direction opposite in each line.The script takes it into account.

<img src="Build pictures/stripe_wiring.png"  width="400" height="150">

Be careful to respect the direction of the strips or some leds wont lit and youll have to unglue the whole strip.

You can use ATX power supplies, youll have to put a GND on the PS_ON pin, but you'll need a lot of them and theyre heavy and full of useless wires as you'll only use 5VDC.

### Frame building

We used a 120 * 71 cm transparent polycarbonate panel with a plexiglass panel in front of leds.
We had to add wood frames in front of and behind the polycarbonate panel as spacers with the plexiglass and the rear panel.

<img src="Build pictures/ledWall_build2.png"  width="1032" height="500">
<img src="Build pictures/ledWall_build3.png"  width="1032" height="500">

## Still to be implemented:
- Simplify variables redefinition for install
- Remote control via WIFI (Acess point mode ?)
- DMX output circuit (may be simply cables)
- LED control via SPI (to enable use of a USB mic)
- Sealing of the Ledwall for outdoor use, removable solution needed (for led repair on site)
- attachment points/ system had to be designed


# LEDWALL
# Author: Som One
#
# ver 05062020 01:40 : implemente:
#			- DMX completement implemente (enfin!) via arduino nano+max485, code ardu = dmx4rpi
#			- galere sur forum :https://forum.arduino.cc/index.php?topic=684814.15
#			a faire :
#			- regler le fait que le dmx ne marche qu'avec un cable XLR classique
#			- optimiser le transfert de texte defilant via DMX
#			- reussir a passer les leds en SPI pour pouvoir utiliser le mic (pour linstant ca kill des leds DANGER!!!)
#
# ver 29122019 01:56 : implemente:
#			- commande pour couleur texte RGB (ex: TC255,0,127)
#			- bars verticales pour VU mettre
#			- animation carres qui grandissent,cercles en negatif
#			- implementation DMX dans firmware arduino (dmx4pi)+affichage debug trame DMX
#			- ajout chargement dossier videos en live (ex: VD/home/pi/ledWall/video/loop)
#			- changement couleur motif en live (variable drawColor, commande CG255,0,255)
#			- ajout de 5 parametres utilises par les DRAW, 5 addresses DMX supplementaire (firmware a jour)
#			- ajout texte defilants aux DRAW
#			
#			a faire:
#			- corriger VU metre plus rapide+ajouter lecture micro
#			- refaire le dessin de la forme d'onde
#			- faire animation triangles qui grandissent
#
#		
# 
# ver 22122019 05:38 : implemente
#			- video plus rapide (loadVideoImageFast)
#			- gestion video chargees en memoire pour supprimer temps de chargement entre videos
#			- reception commandes via serial, en utilisant FTDI sur software serial Arduino (car lib DMX et serial classique ne cohabitent pas!)
#			- dessin formes basiques (cercle, sinus)
#			- gestion playlist, dossier video
#			- adaptation a mur led grand format (60 * 29)
#			
#			a faire:
#			- refaire l'affichage des caracteres (ou attendre reparation 1ere ligne)
#			- mettre les textes defilants en memoire (si pas moyen de les ecrire en DMX)
#			- finir le firware arduino dmx4rpi (test necessaires avec carte DMX)
#
#
# ver 26112019 00:44 : implementes
#			-chargement fichier image en local
#			-chargement et utilisation police au format PNG
#			-support texte defilant depuis image
#			-creation fichier image pour texte defilant
#			-chargement fichier image de video
#			-chargement auto des videos/images presentes dans les dossiers avec les bons noms
#			-scripts pour conversion fichiers
#
#			a faire
#			-redimensionner les images automatiquement
#			-ajouter caracteres speciaux a la police (ne pas oublier la gestion du code ASCII)
#			-modifier les couleurs de maniere plus elaboree
#
#

import time
from neopixel import *
import imageio
from PIL import Image
import os.path
from os import path
import math
import numpy
import wave
import sys
#import RPi.GPIO as GPIO
import serial
import random
#import pyaudio
#import Queue
#q = Queue.Queue()
from struct import unpack


# LED strip configuration:1740 60 29
LED_COUNT      = 1740      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5     # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 30     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0
LED_WIDTH      = 60
LED_HEIGHT     = 29

LED_COUNT      = 2400 
LED_WIDTH      = 60
LED_HEIGHT     = 40

'''
INPUT_PIN=4
GPIO.setmode(GPIO.BCM)
GPIO.setup(INPUT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
'''
TDenable=False
TDpos=0
TDinit=0
TDim={}
mode="DEMO"
prevMode=mode
runLoop=False
v=0
vidzCount=0
cmd_rx=""
textColor=Color(255,255,255)
drawColor=Color(255,255,255)
params=[0,0,0,0,0] #time,opt1,opt2,opt3,opt4


ser = serial.Serial('/dev/ttyUSB0', 9600)
videoPath="/home/pi/ledWall/video/loop"

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)

############################################ COMMANDS #####################################################
def cmd_receive():
	global cmd_rx,ser,TDenable,textColor,mode,prevMode,v,strip,videoPath,drawColor,params
	stopLoop=False
	if (ser.in_waiting !=0):
		msg=ser.readline()
		cmd_rx=msg[:-2]
		
	if (cmd_rx != ""):
		#print (cmd_rx)
		if cmd_rx.find("TD") >=0: # active text defilant
				TDenable=True
		elif cmd_rx.find("TC") >=0:  # change couleur text defilant
			cmd=cmd_rx[2:]				
			if cmd == "R" :
				textColor=Color(0,255,0)
			if cmd == "G" :
				textColor=Color(255,0,0)
			if cmd == "B" :
				textColor=Color(0,0,255)
			if cmd == "W" :
				textColor=Color(255,255,255)
			if cmd_rx.find(",") >=0:
				r=int( cmd[:cmd.find(",")] )	#255,0,127
				cmd=cmd[cmd.find(",")+1:]	#0,127
				g=int( cmd[:cmd.find(",")] )
				b=int( cmd[cmd.find(",")+1:])
				textColor=Color(g,r,b)
		elif cmd_rx.find("CG") >=0:  # change couleur motif
			print ("	Color change")
			cmd=cmd_rx[2:]
			if cmd_rx.find(",") >=0:
				r=int( cmd[:cmd.find(",")] )	#255,0,127
				cmd=cmd[cmd.find(",")+1:]	#0,127
				g=int( cmd[:cmd.find(",")] )
				b=int( cmd[cmd.find(",")+1:])
				drawColor=Color(g,r,b)
		elif cmd_rx.find("BR") >=0:	# change brightness
			cmd=cmd_rx[2:]
			brightness=int(cmd)
			if brightness < 0:brightness=0;
			if brightness >255:brightness=255;
			if brightness >100:brightness=100;
			print("	brightness = "+str(brightness))
			strip.setBrightness(brightness)
		elif cmd_rx.find("DEMO") >=0:  # mode demo
			stopLoop=True
			mode="DEMO"
			if len(cmd_rx) > 4:
				v=int(cmd_rx[4:])-1
			else :
				v=0			
		elif cmd_rx.find("VL") >=0:	# charger 1 video
			stopLoop=True
			mode="LOOP"
			w=int(cmd_rx[2:])
			if w <= vidzCount:
				v=w
		elif cmd_rx.find("DRAW") >=0:	# charger 1 rendu
			stopLoop=True
			mode="DRAW"
			cmd=cmd_rx[4:]
			
			if v != int(cmd):
				print("drawing figure "+str(int(cmd))) 
			v=int(cmd)
			if v <= 0: v=1;

		elif cmd_rx.find("SOUND") >=0: # charger oscillogramme/spectrogramme
			stopLoop=True
			mode="SOUND"

		elif cmd_rx.find("DMX") >=0: # afficher commande DMX (debug)
			print (cmd_rx)

		elif cmd_rx.find("VD") >=0: #charger un autre dossier videos
			stopLoop=True
			videoPath=cmd_rx[2:]
			mode="LOADV"

		elif cmd_rx.find("PC") >=0:  # change parametres
			paramNum=int(cmd_rx[2:cmd_rx.find(",")])
			if paramNum != 0 and paramNum-1 < len(params):
				paramVal=int(cmd_rx[cmd_rx.find(",")+1:])
				print("	Param "+str(paramNum)+" = "+str(paramVal))
				params[paramNum-1]=paramVal

		if mode != prevMode:
			prevMode=mode
			print("########### Switch to mode " + mode)

		cmd_cp=cmd_rx[2:]
		cmd_rx=""
		return cmd_cp,stopLoop
		


############################################# LEDS ########################################################

# Define functions which animate LEDs in various ways.

def colorWipeFast(strip, color=Color(0,0,0), wait_ms=50,display=True):
	line=0;
	"""Wipe color across display a pixel at a time."""
	for i in range(strip.numPixels()):
		
		strip.setPixelColor(i, color)
	if display == True:
		strip.show()	


def ledWrite(x,y,col):
	led=0	
	if y%2 == 0:
		led=x+y*LED_WIDTH
	else:
		led=LED_WIDTH*(y+1)-x-1

	strip.setPixelColor(led,col)
	
	

############################################ TEXT ##################################################


def loadTextDefil(text,offset,dly,color=Color(255,0,0),y_ofst=0):
	textScrollH(text,color)
	loadImageDefil("/home/pi/ledWall/image/test.png",offset,dly,color,y_ofst)

def textScrollH(text="TEST",color=Color(255,0,255)):
	x=60;y=0
	char_xm=5
	char_ym=5
	im = imageio.imread("/home/pi/ledWall/image/char.png") 
	im2 = Image.new("RGB", ((char_xm+1)*len(text)+120,char_ym), "black")
	for l in text:
		if 65 <= ord(l) <= 90:# lettres maj
			char_adr=ord(l)-65	
		if 97 <= ord(l) <= 122:# lettres min
			char_adr=ord(l)-97
		if 48 <= ord(l) <= 57:# chiffres
			char_adr=ord(l)-48 + 26
		if 32 <= ord(l) <= 47: # signes 1
			char_adr=ord(l)+4
		if 58 <= ord(l) <= 64: # signes 2
			char_adr=ord(l)-6
		if 91 <= ord(l) <= 96: # signes 3
			char_adr=ord(l)-32
		
		#print (char_adr)	
		for i in range(char_xm):
			for j in range(char_ym):
				#col=Color(im[i+char_adr*char_xm,j][1],im[i+char_adr*char_xm,j][0],im[i+char_adr*char_xm,j][2])
				if im[i+char_adr*char_xm,j][1] != 0:
					
					im2.putpixel( (x+j,y+i) ,(255,0,0) )
				
		
		x=x+char_xm+1
	
	im2.save("/home/pi/ledWall/image/test.png", format="png")

			
def printChar(car,x=0,y=0,color=Color(255,0,255)):
	
	char_xm=5
	char_ym=5
	im = imageio.imread("/home/pi/ledWall/image/char.png") 
	
	for l in car:
		if 65 <= ord(l) <= 90:# lettres maj
			char_adr=ord(l)-65	
		if 97 <= ord(l) <= 122:# lettres min
			char_adr=ord(l)-97
		if 48 <= ord(l) <= 57:# chiffres
			char_adr=ord(l)-48 + 26
		if 32 <= ord(l) <= 47: # signes 1
			char_adr=ord(l)+4
		if 58 <= ord(l) <= 64: # signes 2
			char_adr=ord(l)-6
		if 91 <= ord(l) <= 96: # signes 3
			char_adr=ord(l)-32
	
		#print (char_adr)	
		for i in range(char_xm):
			for j in range(char_ym):
				#col=Color(im[i+char_adr*char_xm,j][1],im[i+char_adr*char_xm,j][0],im[i+char_adr*char_xm,j][2])
				
				if im[i+char_adr*char_xm,j][1] != 0:
					col=color
				else:
					col=Color(0, 0, 0)
				ledWrite(j+x,i,col)
				#if i+y %2 != 0:
					#ledWrite(#strip.setPixelColor(x+j+LED_WIDTH*(i+y),col)	
				#else:
					#strip.setPixelColor(LED_WIDTH*(i+1+y)-j-1-x,col)

		
		x=x+char_xm+1
	strip.show()

def loadTD():
	global TDenable,TDpos,TDinit,TDim
	y_ofst=0
	offset=2
	rcv=""
	

	if (ser.in_waiting !=0):
		rcv,interrupt=cmd_receive()
		if interrupt == True:
			runLoop=False
			#break

	if TDenable == True:
		if TDinit == 0:
			TDinit=1
			
			if rcv != "":
				textScrollH(rcv,textColor)
			print ("	Start TD") #, TD = "+ str(TDenable)+", TDpos = "+ str(TDpos))
			TDim = imageio.imread("/home/pi/ledWall/image/test.png")
			TDpos = 0
			
			rcv=""
			
		TDpos=TDpos+offset
		imgWidth=TDim.shape[1]
		imgHeight=TDim.shape[0]

		if(LED_WIDTH+TDpos >=imgWidth+2):
			TDinit=0
			TDpos=0
			TDenable = False	
			print ("	Stop TD")
		else:
			for i in range(imgHeight):
				for j in range(LED_WIDTH):
					if textColor == Color(0,0,0):c0=Color(255,255,255);
					else: c0=Color(0,0,0);
					if ((TDim[i,j+TDpos][0] != 0)):
						#col=Color(im[i,j+o][1],im[i,j+o][0],im[i,j+o][2])
						col=textColor
					else:
						col=c0
					if i%2 == 0:
						strip.setPixelColor(j+LED_WIDTH*(i+y_ofst),col)	
					else:
						strip.setPixelColor(LED_WIDTH*(i+1+y_ofst)-j-1,col)	

############################################# IMAGES ###################################
def loadImage(path):
	im = imageio.imread(path)
	#im = ndimage.imread(path)
	#print(im.shape[1])
	
	for i in range(LED_HEIGHT):
		for j in range(LED_WIDTH):
			if i%2 == 0:
				strip.setPixelColor(j+LED_WIDTH*i,Color(im[i,j][1],im[i,j][0],im[i,j][2]))	
			else:
				strip.setPixelColor(LED_WIDTH*(i+1)-j-1,Color(im[i,j][1],im[i,j][0],im[i,j][2]))
		#time.sleep(0.002)	
	strip.show()
	
def loadImageOld(path):
	im = imageio.imread(path)
	#print(im.shape[1])
	
	for i in range(LED_HEIGHT):
		for j in range(LED_WIDTH):
			if i%2 == 0:
				strip.setPixelColor(j+LED_WIDTH*i,Color(im[i,j][1],im[i,j][0],im[i,j][2]))	
			else:
				strip.setPixelColor(LED_WIDTH*(i+1)-j-1,Color(im[i,j][1],im[i,j][0],im[i,j][2]))
		#time.sleep(0.002)	
	strip.show()

def loadImageDir(path):
	index=1
	path1=path+"/img"+"%03d" % (index,)+".png"
	print ("loading images in "+ path )
	

	while (os.path.isfile(path1)):
		print ("image " + path1[-10:])
		loadImage(path1)
		time.sleep(5)
		index=index+1
		path1=path+"/img"+"%03d" % (index,)+".png"

def loadImageDefil(path,offset,dly,color=Color(255,0,0),y_ofst=0):
	im = imageio.imread(path)
	imgWidth=im.shape[1]
	#print(imgWidth)
	o=0
	
	while(LED_WIDTH+o<imgWidth):
			
		for i in range (im.shape[0]):
			for j in range (LED_WIDTH):
				if ((im[i,j+o][0] != 0) or (im[i,j+o][1] != 0) or (im[i,j+o][2] != 0)):
					#col=Color(im[i,j+o][1],im[i,j+o][0],im[i,j+o][2])
					col=color	
				else:
					col=Color(0,0,0)
				if i%2 == 0:
					strip.setPixelColor(j+LED_WIDTH*(i+y_ofst),col)	
				else:
					strip.setPixelColor(LED_WIDTH*(i+1+y_ofst)-j-1,col)
		o=o+offset	
		strip.show()
		time.sleep(dly)

############################################# VIDEO ###################################


def loadVideoPlaylist(path="/home/pi/ledWall/video_playlist.txt"):
	print ("loading video playlist: "+ path )
	file = open(path, "r") 
	l=1
	for line in file: 
		if '\n' in line :
			vpath=line[:len(line)-1]
		else:
			vpath=line
		print(" "+str(l)+" - "+vpath) 
		loadVideoImageFast(vpath)
		l=l+1
	file.close()

def loadVideoDir(path):
	index=1
	path1=path+"/vid"+"%02d" % (index,)
	print ("########### Loading vidz in "+ path )
	while (os.path.isdir(path1)):
		loadVideoImageFast(path1)
		index=index+1
		path1=path+"/vid"+"%02d" % (index,)
def loadVideo(path):
	l=0
	im_arr = {}
	im_arrtt = {}
	a=255;b=255;c=255; 
	
	index=1
	while (os.path.isfile(path+"/img-"+"%05d" % (index,)+".png")):
		with Image.open(path+"/img-"+"%05d" % (index,)+".png") as image: 
			im_arrt=numpy.fromstring(image.tobytes(),dtype=numpy.uint8)
			im_arr[l]=im_arrt.reshape((image.size[1],image.size[0],3))
		#print im_arr[l]   
		l=l+1
		index=index+1
	return im_arr,l


def fcount(path, map = {}):
  count = 0
  for f in os.listdir(path):
    child = os.path.join(path, f)
    if os.path.isdir(child):
      child_count = fcount(child, map)
      count += child_count + 1 # unless include self
  map[path] = count
  return count



def loadVideos(path):
	videos={}
	videosLen={}
	index=1
	path1=path+"/vid"+"%02d" % (index,)
	vidNumbC=0;

	print ("########### Loading vidz in "+ path )	
	#for path, subdirs, files in os.walk(path):
    	#	for name in subdirs:
       	#		if str(subdirs).find("vid") >= 0:
	#			vidNumbC=vidNumbC+1
	vidNumbC=fcount(path)
	colorWipeFast(strip, Color(0, 0, 0));printChar("LOADING...",3,0,Color(0, 255, 0));time.sleep(0.1);

	while (os.path.isdir(path1)):
		videos[index-1],videosLen[index-1]=loadVideo(path1)
		car="vid "+str(index)+"/"+str(vidNumbC)
		colorWipeFast(strip, Color(0, 0, 0));time.sleep(0.1);printChar(car,3,0,Color(0, 255, 0));
		print("video " + str(index)+"/"+str(vidNumbC) + " loaded")
		index=index+1
		path1=path+"/vid"+"%02d" % (index,)
	
	colorWipeFast(strip, Color(0, 0, 0));
	#tab=[7,8,9]
	#for v in tab:
	#	playVideo(videos[v-1],videosLen[v-1],v)

	return videos,videosLen,index-1

def playVideo(video,l,number):
	runLed=True
	looped=False
	global cmd_rx,TDenable,textColor,TDpos
	
	clim=0
	init=0
	rcv=""
	o=TDpos
	imgWidth=0
	imgHeight=0
	
	while (runLed == True):	
		print ("play video " + str(number))
		for k in range(l):
			for i in range(LED_HEIGHT):
				for j in range(LED_WIDTH):
					
					if TDenable == True and LED_WIDTH+o<imgWidth and i < imgHeight:
						if ((im[i,j+o][0] != 0)):
							#col=Color(im[i,j+o][1],im[i,j+o][0],im[i,j+o][2])
							col=textColor
						else:
							col=Color(0,0,0)
						if i%2 == 0:
							strip.setPixelColor(j+LED_WIDTH*(i+y_ofst),col)	
						else:
							strip.setPixelColor(LED_WIDTH*(i+1+y_ofst)-j-1,col)
					else:
						a=int(video[k][i,j][1]);
						b=int(video[k][i,j][0]);
						c=int(video[k][i,j][2])
						color=Color(a,b,c)
						
						if i%2 == 0:
							strip.setPixelColor(j+LED_WIDTH*i,color)	
						else:
							strip.setPixelColor(LED_WIDTH*(i+1)-j-1,color)
						
			strip.show()
			'''
			if (GPIO.input(INPUT_PIN) == True):
				runLed=False
				break
			'''
			if (ser.in_waiting !=0):
				rcv,interrupt=cmd_receive()
				if interrupt == True:
					runLed=False
					break
				

			if TDenable == True:
				if init == 0:
					init=1
					
					if rcv != "":
						textScrollH(rcv,textColor)
					print ("	Start TD") #, TD = "+ str(TDenable)+", TDpos = "+ str(TDpos))
					im = imageio.imread("/home/pi/ledWall/image/test.png")
					imgWidth=im.shape[1]
					imgHeight=im.shape[0]
					o=TDpos
					offset=2
					y_ofst=0
				
					
				o=o+offset
				TDpos=o
				if(LED_WIDTH+o>=imgWidth):
					init=0
					TDpos=0
					TDenable = False	
					print ("	Stop TD")				
		if (looped != True):
			runLed=False	

def loadVideoImageFast(path,looped=False):
	imgs = {} 
	img_path =path 
	l=0
	im_arr = {}
	im_arrtt = {}
	a=255;b=255;c=255; 
	
	index=1
	while (os.path.isfile(path+"/img-"+"%05d" % (index,)+".png")):
		with Image.open(path+"/img-"+"%05d" % (index,)+".png") as image: 
			im_arrt=numpy.fromstring(image.tobytes(),dtype=numpy.uint8)
			im_arr[l]=im_arrt.reshape((image.size[1],image.size[0],3))
		#print im_arr[l]   
		l=l+1
		index=index+1
	#print (l, "images loaded")
	runLed=True
	
	while (runLed == True):	
		print ("play video " + path[-2:])
		for k in range(l):
			for i in range(LED_HEIGHT):
				for j in range(LED_WIDTH):
					a=int(im_arr[k][i,j][1]);
					b=int(im_arr[k][i,j][0]);
					c=int(im_arr[k][i,j][2])
					color=Color(a,b,c)
					
					if i%2 == 0:
						strip.setPixelColor(j+LED_WIDTH*i,color)	
					else:
						strip.setPixelColor(LED_WIDTH*(i+1)-j-1,color)
				
			strip.show()
		
			if (ser.in_waiting !=0):
				msg=ser.readline()
				cmd_rx=msg[:-2]
				#loadVideoImageFast(ser.readline(),True)
				runLed=False
				break	
		if (looped != True):
				runLed=False
		
			
	



############################################ SOUND ###########################################
# Return power array index corresponding to a particular frequency
def piff(val):
    return int(2 * chunk * val / sample_rate)


# Return int value for volume for fregquency range
def volume_frequency_range(power, freq_low, freq_high):
    try:
        volume = int(numpy.mean(power[piff(freq_low):piff(freq_high):1]))
        return volume
    except:
        return 0


def calculate_levels(data, chunk, sample_rate):
    global matrix

    # Convert raw data (ASCII string) to numpy array
    data = unpack("%dh" % (len(data) / 2), data)
    data = numpy.array(data, dtype='h')

    # Apply FFT - real data
    fourier = numpy.fft.rfft(data)
    # Remove last element in array to make it the same size as chunk
    fourier = numpy.delete(fourier, len(fourier) - 1)
    # Find average 'amplitude' for specific frequency ranges in Hz
    power = numpy.abs(fourier)
    matrix[0] = volume_frequency_range(power, 0, 156)
    matrix[1] = volume_frequency_range(power, 156, 313)
    matrix[2] = volume_frequency_range(power, 313, 625)
    matrix[3] = volume_frequency_range(power, 625, 1250)
    matrix[4] = volume_frequency_range(power, 1250, 2500)
    matrix[5] = volume_frequency_range(power, 2500, 5000)
    matrix[6] = volume_frequency_range(power, 5000, 10000)
    matrix[7] = volume_frequency_range(power, 10000, 20000)

    # Tidy up column values for the LED matrix
    matrix = numpy.divide(numpy.multiply(matrix, weighting),1000000)
    # Set floor at 0 and ceiling at 8 for LED matrix
    # matrix = matrix.clip(0, 8)
    matrix = matrix.clip(0, 24)
    return matrix

def audio_callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    
    # Fancy indexing with mapping creates a (necessary!) copy:
    #q.put(indata[::10,1])

def readMic(slp=0.02):
	global runLoop,params
	runLoop == True

	no_channels = 1
	sample_rate = 44100
	chunk = 3072
	device = 2
	##p = pyaudio.PyAudio()
	##stream = p.open(format = pyaudio.paInt16,channels = 1,rate = 44100,input = True,frames_per_buffer = chunk,input_device_index = device)
	#stream = sd.InputStream(device=device, channels=1,samplerate=sample_rate, callback=audio_callback)

	matrix = [0, 0, 0, 0, 0, 0, 0, 0]
	power = []
	weighting = [2, 8, 8, 16, 16, 32, 32, 64]
	weighting = [x*8 for x in weighting]	

	while runLoop == True:
		data = stream.read(chunk)
		matrix=calculate_levels(data,chunk,sample_rate)
		drawBars(matrix,4)
		
	##stream.stop_stream()
        ##stream.close()
        ##p.terminate()			

def loadSoundSPAN(path):
	print ("play sound " + path)
	while (os.path.isfile(path)):
		#print (wv.read(path))
		sig= wave.open(path,'rb')
		#w,sr=librosa.load(path)
		wfm=numpy.fromstring(sig.readframes(-1),dtype=int)
		cnt=0
		sample={}
		#meansmp={};mnt=0
		#for smp in wfm:
		for i in range(int(sig.getnframes())):
			if (cnt == 60):
				for j in range(LED_WIDTH):
					for i in range(LED_HEIGHT):
						ledWrite(j,i, Color(0,0,0))		
					color=Color(255,0,0)
					ledWrite(j,sample[j],color)
				strip.show()
					#print sample
					#mnt=0
				#else:
					#meansmp[mnt]=int(sum(sample) / len(sample)) 
					#mnt=mnt+1
				cnt=0			
			else:
				#sample[cnt]=int(wfm[smp][cnt])
				val=int(14.0*6.0*float(wfm[i])/2147483648.0)
				#print (wfm[i] ,val )
				sample[cnt]=14+val
				cnt=cnt+1
		sig.close()			


def drawBars(height,width=4):
	#if height >= LED_HEIGHT:
	#	height=LED_HEIGHT
	#if width >= LED_WIDTH:
	#	width=LED_WIDTH
	
	barCount=8
	if drawColor ==   Color(255,0,0): color=1;
	elif drawColor == Color(0,0,255): color=2;
	elif drawColor == Color(0,255,0): color=3;
	elif drawColor == Color(255,255,255): color=4;
	elif drawColor == Color(0,0,0): color=5;
	else:  color=1;
	if color==1 :
		c1=Color(255,0,0)
		c2=Color(100,255,0)
		c3=Color(0,255,0)
		c4=Color(0, 0, 0)
	elif color==2 :
		c1=Color(0,0,255)
		c2=Color(0,255,100)
		c3=Color(0,255,255)
		c4=Color(0, 0, 0)
	elif color==3 :
		c1=Color(0,255,0)
		c2=Color(0,255,255)
		c3=Color(0,255,100)
		c4=Color(0, 0, 0)
	elif color==4 :
		c1=Color(255,255,255)
		c2=Color(0,0,100)
		c3=Color(0,0,255)
		c4=Color(0, 0, 0)
	elif color==5 :
		c1=Color(0,0,0)
		c2=Color(50,50,50)
		c3=Color(100,100,100)
		c4=Color(255,255,255)
	else:
		c1=Color(255,255,255)
		c2=Color(0,0,100)
		c3=Color(0,0,255)
		c4=Color(0, 0, 0)
	
	colorWipeFast(strip, c4,0,False);
	'''
	for k in range(barCount):
		#for o in range(LED_HEIGHT):
		#	for p in range(width):
		#		ledWrite(k*(width+4)+p,o, Color(0,0,0))
			
		for i in range(height[k]):
			for j in range(width):
				#for k in range(12):
				#	ledWrite((width+1)*k+j,LED_HEIGHT-height, Color(0,255,0))
				#if j > LED_HEIGHT-height:
				
				if i >= LED_HEIGHT-2:
					col=c3
				elif LED_HEIGHT-2   > i >= LED_HEIGHT-6:
					col=c2
				else:
					col=c1
				
				ledWrite(k*(width+4)+j,LED_HEIGHT-i, col)
	'''			
	for i in range(LED_HEIGHT):
		for j in range(width):
			for k in range(barCount):
			
				if i >= LED_HEIGHT-2:
					col=c3
				elif LED_HEIGHT-2   > i >= LED_HEIGHT-6:
					col=c2
				else:
					col=c1

				if height[k] >= i:
					ledWrite(k*(width+4)+j,LED_HEIGHT-i, col)
	
				
	loadTD()	
	
	strip.show()
	slpt=float( (params[0]+1)*0.025 )
	time.sleep(slpt)


def testBars(slp=0.02):
	global runLoop,params
	runLoop == True

	w, h = 8, 50;
	vals = [[random.randint(0, 39) for x in range(w)] for y in range(h)] 

	for n in range(h):
		
		drawBars(vals[n],4)
		if runLoop == False:
			break
		


############################################# DEMO & DEBUG ###################################

def drawTriangle(y0,size,col):
	global params
	x0=30-size
	y1=y0+size
	center=params[2]

	if col == Color(0,0,0):
		c0=Color(255, 255, 255)
		c1=Color(10,10,10)
	else:
		c0=Color(0, 0, 0)
		b1=int(col&0xFF/10);g1=int( (col&0x00FF00/0xFF-1) /10);r1=int((col&0xFF0000/0xFFFF)/10)
		c1=Color(r1,g1,b1) 
	
	colorWipeFast(strip,c0 ,False)
	for i in range(size+1):
		if center == 1:
			ledWrite(30,y0-size+i,c1)
			ledWrite(30,y0+size-i,c1)
			ledWrite(30-i,y0,c1)
			ledWrite(30+i,y0,c1)


		ledWrite(x0+i,y0-i,col)
		ledWrite(x0+i,y0+i,col)
		ledWrite(30+i,y0-size+i,col)
		ledWrite(30+i,y0+size-i,col)

		
	loadTD()	
	strip.show()
	
def growingTriangle(col=Color(255,0,0),direction=0,gradient=0):
	
	rg=20
	for k in range(rg):
		global params
		
		if params[1] == 0:
			drawTriangle(20,k,Color(255,255,255))
		else:
			drawTriangle(20,rg-k,Color(255,255,255))

		time.sleep(float((params[0]+1)*0.03))

			

def growingSquares(d=0.050,direction=0,gradient=0,dstruct=0):
	global drawColor,runLoop,params
	runLoop=True
	x=30
	y=19
	maxsize=40

	if direction > 1: direction=1;
	if gradient > 1: gradient=1;
	if dstruct > 1: dstruct=1;

	if direction==0:	
		size=4
		while size<maxsize and runLoop==True:
			col=drawColor
			if drawColor == Color(0, 0, 0):
				c0=Color(255, 255, 255)
			else:
				c0=Color(0, 0, 0)
			colorWipeFast(strip, c0,0,False);
			if gradient==1:
				col=Color(255, 255*size/26, 0)	
			for i in range(size):
				if dstruct != 1:
					ledWrite(x-size/2+i,y-size/2+1,col)
					ledWrite(x-size/2+i,y+size/2,col)
					ledWrite(x-size/2,y+size/2-i,col)
					ledWrite(x+size/2,y-size/2+i+1,col)
				else:
					ledWrite(x-(32-size)/2+i,y-size/2+1,col)
					ledWrite(x-(32-size)/2+i,y+size/2,col)
					ledWrite(x-size/2,y+(32-size)/2-i,col)
					ledWrite(x+size/2,y-(32-size)/2+i+1,col)
			loadTD()
										
			strip.show()
			
			time.sleep(float((params[0]+1)*0.03))
			size=size+2
	else:
		size=maxsize-2
		while size>0 and runLoop==True:
			col=drawColor
			if drawColor == Color(0, 0, 0):
				c0=Color(255, 255, 255)
			else:
				c0=Color(0, 0, 0)
			colorWipeFast(strip, c0,0,False);
			for i in range(size):
				if gradient==1:
					col=Color(255, 255*size/26, 0)	
				if dstruct != 1:
					ledWrite(x-size/2+i,y-size/2+1,col)
					ledWrite(x-size/2+i,y+size/2,col)
					ledWrite(x-size/2,y+size/2-i,col)
					ledWrite(x+size/2,y-size/2+i+1,col)
				else:
					ledWrite(x-(32-size)/2+i,y-size/2+1,col)
					ledWrite(x-(32-size)/2+i,y+size/2,col)
					ledWrite(x-size/2,y+(32-size)/2-i,col)
					ledWrite(x+size/2,y-(32-size)/2+i+1,col)
			loadTD()	
			strip.show()
			time.sleep(float((params[0]+1)*0.03))
			size=size-2


def circle(a,b,r,color=Color(255,0,0),fill=0):
	width, height = LED_WIDTH, LED_HEIGHT
	EPSILON = 3.8
	e2=4
	
	for y in range(height):
		for x in range(width):
			if abs((x-a)**2 + (y-b)**2 - r**2) < EPSILON**2:
				ledWrite(x,y,color)
			if abs((x-a)**2 + (y-b)**2 - r**2) < e2**2 and fill==1:
				ledWrite(x,y,color)

def sine():
	global drawColor,runLoop,params
	runLoop=True

	for j in range(LED_WIDTH):
		if drawColor==Color(0,0,0):
			c0=Color(255,255,255)
		else:
			c0=Color(0,0,0)
		colorWipeFast(strip, c0,0,False);	
		for i in range(LED_WIDTH):
			ledWrite(i,int(round(LED_HEIGHT*(1+math.sin((params[4]+1)*(i+j)*math.pi/LED_WIDTH))/2)),drawColor)
		
		loadTD()	
		strip.show()
		if runLoop==False:
			break
			

def circles_inner(color1,color2,t=0.01):
	global drawColor,runLoop,params
	runLoop=True
	max_diam=20
	y0=20
	colorWipeFast(strip, color2,0,False);
	for i in range(max_diam):
		circle(30,y0,i+1,color1,1);
		#if i>0:
		#	circle(30,14,i,color2)
			
		#else:
		#	circle(30,14,max_diam,color2)
		loadTD()
		strip.show()
		if runLoop==False:
			break
		time.sleep(float((params[0]+1)*0.01))
		
	
def circles(t=0.1,color2=Color(0,0,0)):
	global params
	max_diam=19
	center=True
	trail=True
	numtrail=1
	global drawColor,runLoop,params
	runLoop=True
	y0=20
	colorWipeFast(strip, Color(0, 0, 0),0,False)
	for i in range(max_diam):
		if params[1] >= 1:trail=True;
		else: trail=False;
		if params[2] >= 1:center=True;
		else: center=False;
		color1=drawColor

		if color1==Color(255,0,0):
			c2=Color(50,0,0);c3=Color(10,0,0);
		elif color1==Color(0,255,0):
			c2=Color(0,50,0);c3=Color(0,10,0);
		elif color1==Color(0,0,255):
			c2=Color(50,0,50);c3=Color(10,0,10);
		elif color1==Color(0,0,0):
			c2=Color(0,0,0);c3=Color(0,0,0);
		elif color1==Color(255,255,255):
			c2=Color(100,100,100);c3=Color(20,20,20);
		else:
			c2=Color(0,50,0);c3=Color(0,10,0);
		
		if trail == True:
			for j in range (numtrail):
				if color1==Color(255,0,0):
					c1=Color(30-3*j,0,0)
				elif color1==Color(0,255,0):
					c1=Color(0,30-3*j,0)
				elif color1==Color(0,0,255):
					c1=Color(30-3*j,0,30-3*j)
				elif color1==Color(0,0,0):
					c1=Color(0,0,0)
				elif color1==Color(255,255,255):
					c1=Color(100,100,100)
				else:
					c1=Color(0,30-3*j,0)
				if j%2 == 0:
					if i > j + 2:
						circle(30,y0,i-2-j,c1)
					if i > j + 3:
						circle(30,y0,i-3-j,color2)
					if i == 0:
						circle(30,y0,max_diam-3-j,color2)
		
		if center == True:
			if i%2 == 0:
				circle(30,y0,1,color2)
				circle(30,y0,0,c2)
			else:
				circle(30,y0,0,color2)
				circle(30,y0,1,c3)		
		circle(30,y0,i+1,color1);
		if i>0:
			circle(30,y0,i,color2)
		else:
			circle(30,y0,max_diam,color2)

		
		loadTD()
		
		strip.show()
		if runLoop==False:
			break
		time.sleep(float((params[0])*0.01))

def ludoNico(loop):
	for i in range (loop):
		loadImage("/home/pi/ledWall/image/bdpg1.png")
		time.sleep(0.1)
		loadImage("/home/pi/ledWall/image/bdpg2.png")
		time.sleep(0.1)


def bdp(color=Color(255,0,0),dly=0):
	printChar("balayeurs",3,0,color);time.sleep(dly);colorWipeFast(strip, Color(0, 0, 0))
	printChar("du",30,0,color);time.sleep(dly);colorWipeFast(strip, Color(0, 0, 0))
	printChar("plafond",10,0,color);time.sleep(dly);colorWipeFast(strip, Color(0, 0, 0))

def bdpDemo(dly=0.5):
	bdp(Color(255,0,0),dly)
	bdp(Color(0,255,0),dly)
	bdp(Color(0,0,255),dly)
	bdp(Color(255,255,255),dly)

def colorTest():
	colorWipeFast(strip, Color(0, 255, 0));time.sleep(1)
	colorWipeFast(strip, Color(255, 0, 0));time.sleep(1)
	colorWipeFast(strip, Color(0, 0, 255));time.sleep(1)
	colorWipeFast(strip, Color(0, 0, 0));time.sleep(1)

def debug():
	for j in range(LED_WIDTH):
		for i in range(LED_HEIGHT):
			ledWrite(j,i,Color(255, 0, 0))
		strip.show()
	

def initLed():
	colorTest()
	#loadTextDefil("BALAYEURS DU PLAFOND",2,0,Color(0,255,0),10)
	#bdpDemo(1);



############################################ MAIN ####################################################	

# Main program logic follows:
if __name__ == '__main__':
	strip.begin()
	initLed()

	vidz,vidzLen,vidzCount=loadVideos(videoPath)	
	print("########### Starting with mode " + mode)

	print ('(Press Ctrl-C to quit.)')
	while True:
		
		cmd_receive()
			
		if mode == "DEMO":
			if v >= vidzCount:
				v=1
			else:
				v=v+1
			#print ("v = "+ str(v)+ " vidzcount = "+ str(vidzCount))
			playVideo(vidz[v-1],vidzLen[v-1],v)

		if mode == "LOOP":
			if v==0:v=1;
			playVideo(vidz[v-1],vidzLen[v-1],v)

		if mode == "SOUND":
			#loadSoundSPAN("/home/pi/ledWall/DollazMono.wav")
			toto=1

		if mode == "LOADV":
			vidz,vidzLen,vidzCount=loadVideos(videoPath)
			v=0
			mode = "DEMO"
			cmd_rx="DEMO"

		if mode == "DRAW":
			if v == 1 :
				if drawColor == Color(0,0,0):
					circles_inner(drawColor,Color(255,255,255))
				else:
					circles(0)
			elif v == 2 :
				growingSquares(0.02,params[1],params[2],params[3])
			elif v == 3 :
				growingTriangle();
			elif v == 4 :
				##readMic(0.05);
				testBars(0.05);
				toto=1	
			elif v == 5 :
				sine();	
			elif v == 6 :
				tot=1;			

		if mode == "TEST":
			#loadImageDefil("/home/pi/ledWall/image/bdptxt.png",2,0.05,Color(0,0,255))
			#loadImageDir("/home/pi/ledWall/image")
			#loadVideoDir("/home/pi/ledWall/video")
			loadVideoPlaylist()
			#loadVideoImageFast("/home/pi/ledWall/video/vid01")
			#loadImage("/home/pi/mario_r.png")

	colorWipeFast(strip, Color(0, 0, 0));
		
